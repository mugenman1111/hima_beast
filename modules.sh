#!/bin/bash

dir=${HOME}/kernel/m9/Hima_beast/
dest=${HOME}/kernel/m9/modules/


find "$dir" -name "*.ko" -exec cp -v {} $dest \; -printf "Found .ko Module\n"
